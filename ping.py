import discord
from discord.ext import commands
import datetime
import time
from random import choice, randint
class Commands:
    

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def ping(self, ctx):
        """Pong."""
        t1 = time.perf_counter()
        await self.bot.send_typing(ctx.message.channel)
        t2 = time.perf_counter()
        thedata = ("**Pong! :ping_pong: **\nIt Took " + str(round((t2-t1)*1000)) + "ms to send this message")
        data = discord.Embed(description=thedata, colour=discord.Colour(value=0x800000))

        await self.bot.say(embed=data)


def setup(bot):
    n = Commands(bot)
    bot.add_cog(n)
    print('Ping has loaded')
